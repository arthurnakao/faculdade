Rails.application.routes.draw do
  resources :periods
  devise_for :users, path: '', path_names: {sign_in: 'login', sign_out: 'logout', password: 'senha', confirmation: 'confirmacao', unlock: 'desbloquear', registration: 'registrar', sign_up: 'cadastrar' }
  
  post '/student_lessons/new' #É Necessário para manter a /student_lessons/registrar funcionando

  root 'pages#home'
  get 'pages/home'
  get 'pages/homeadmin'
  get 'pages/homestandard'
  get 'pages/homeprofessor'
  get 'pages/homedirector'

  scope path_names: {new: 'registrar', edit: 'editar'} do
    resources :users
    resources :student_lessons
    resources :student_classrooms
    resources :subject_courses
    resources :classrooms
    resources :lessons
    resources :students
    resources :teachers
    resources :subjects
    resources :courses
    resources :departments
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  end
end
