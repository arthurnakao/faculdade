class CreateStudentClassrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :student_classrooms do |t|
      t.references :student, foreign_key: true
      t.references :classroom, foreign_key: true

      t.timestamps
    end
  end
end
