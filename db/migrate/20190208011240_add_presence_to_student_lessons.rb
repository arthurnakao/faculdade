class AddPresenceToStudentLessons < ActiveRecord::Migration[5.2]
  def change
    add_column :student_lessons, :presence, :boolean
  end
end
