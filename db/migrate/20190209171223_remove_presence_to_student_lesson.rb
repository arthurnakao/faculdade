class RemovePresenceToStudentLesson < ActiveRecord::Migration[5.2]
  def change
    remove_column :student_lessons, :presence, :boolean
  end
end
