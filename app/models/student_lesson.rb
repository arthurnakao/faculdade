class StudentLesson < ApplicationRecord
  belongs_to :student
  belongs_to :lesson

  validates :student_id, presence: true, uniqueness: {:scope => :lesson_id}
  validates :lesson_id, presence: true
end
