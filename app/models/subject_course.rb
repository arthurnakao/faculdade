class SubjectCourse < ApplicationRecord
  belongs_to :course
  belongs_to :subject

  validates :subject_id, presence: true, uniqueness: {:scope => :course_id}
  validates :course_id, presence: true
end
