class Period < ApplicationRecord
    has_many :classrooms

    validates :number, presence: true, uniqueness: true
end
