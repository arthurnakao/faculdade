class User < ApplicationRecord

  has_one :teacher

  validates :name, :photo, :password_confirmation, presence: true

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  mount_uploader :photo, PhotoUploader

  enum kind: {
    standard: 0,
    admin: 1,
    professor: 2,
    director: 3
  }
end
