class Student < ApplicationRecord
  belongs_to :course

  has_many :student_classrooms
  has_many :classrooms, through: :student_classrooms

  has_many :student_lessons
  has_many :lessons, through: :student_lessons


  validates :name, presence: true
  validates :registration, presence: true, numericality: true
end
