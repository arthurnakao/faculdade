class Lesson < ApplicationRecord
  belongs_to :classroom

  has_many :student_lessons
  has_many :students, through: :student_lessons

  #accepts_nested_attributes_for :student_lessons

  validates :duration, presence: true, numericality: { only_integer: true }
  
end
