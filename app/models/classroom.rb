class Classroom < ApplicationRecord
  belongs_to :period
  belongs_to :teacher
  belongs_to :subject
  has_many :lessons
  has_many :student_classrooms
  has_many :students, through: :student_classrooms

  validates :number, presence: true, numericality: { only_integer: true }
end