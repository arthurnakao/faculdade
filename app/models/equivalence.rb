class Equivalence < ApplicationRecord
    belongs_to :subject
    belongs_to :discipline, class_name: "Subject"
end