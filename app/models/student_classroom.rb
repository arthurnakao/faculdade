class StudentClassroom < ApplicationRecord
  belongs_to :student
  belongs_to :classroom

  validates :student_id, presence: true, uniqueness: {:scope => :classroom_id}
  validates :classroom_id, presence: true
end
