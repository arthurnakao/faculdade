class Subject < ApplicationRecord
  belongs_to :department, optional: true
  has_many :classrooms

  has_many :subject_courses
  has_many :courses, through: :subject_courses

  #SELF RELATION
  has_many :equivalences
  has_many :disciplines, through: :equivalences, source: :discipline


  validates :name, presence: true
  validates :workload, presence: true, numericality: { only_integer: true }
end