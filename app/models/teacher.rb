class Teacher < ApplicationRecord
  belongs_to :department
  has_many :classrooms
  belongs_to :user

  validates :name, presence: true
end
