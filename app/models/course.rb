class Course < ApplicationRecord
  belongs_to :department
  has_many :students

  has_many :subject_courses
  has_many :subjects, through: :subject_courses


  validates :name, presence: true, uniqueness: true
end
