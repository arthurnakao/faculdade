class Ability
  include CanCan::Ability

  def initialize(user)

    if user.standard?
      can :read, :all
      can :destroy, User, id: user.id 
      can :update, User, id: user.id
    end

    if user.admin?
      can :manage, :all
    end

    if user.professor?
      can :read, :all
      can :destroy, User, id: user.id
      can :update, User, id: user.id
      can [:create, :update, :destroy, :read], Lesson
      can :create, StudentLesson
    end

    if user.director?
      can [:create, :update, :destroy, :read], Department
      can [:create, :update, :destroy, :read], Subject
      can [:create, :update, :destroy, :read], Classroom
      can [:update, :read], Teacher
      can [:update, :read], Student
      can :read, :all
      can :create, StudentClassroom
      can :create, SubjectCourse
    end

    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
