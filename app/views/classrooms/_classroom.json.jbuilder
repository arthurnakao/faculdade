json.extract! classroom, :id, :number, :period_id, :teacher_id, :subject_id, :created_at, :updated_at
json.url classroom_url(classroom, format: :json)
