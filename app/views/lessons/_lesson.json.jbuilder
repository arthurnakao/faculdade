json.extract! lesson, :id, :time, :duration, :classroom_id, :created_at, :updated_at
json.url lesson_url(lesson, format: :json)
