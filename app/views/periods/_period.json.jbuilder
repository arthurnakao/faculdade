json.extract! period, :id, :number, :created_at, :updated_at
json.url period_url(period, format: :json)
