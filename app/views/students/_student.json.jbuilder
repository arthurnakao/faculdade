json.extract! student, :id, :name, :registration, :course_id, :created_at, :updated_at
json.url student_url(student, format: :json)
