class StudentLessonsController < ApplicationController
    def new
        @student_lesson = StudentLesson.new
        @students = Student.all
    end

    def create
        @student_lesson = StudentLesson.new(student_lesson_params)
        respond_to do |format|
            if @student_lesson.save
                format.html { redirect_to new_student_lesson_path notice: 'Lesson and student was successfully created.' }
                format.json { render :show, status: :created, location: @student_lesson }
            else
                format.html { render :new }
                format.json { render json: @student_lesson.errors, status: :unprocessable_entity }
            end
        end
    end

    def show
    end

    private
    def student_lesson_params
        params.require(:student_lesson).permit(:student_id, :lesson_id)
    end
end
