class SubjectCoursesController < ApplicationController

    def new
        @subject_course = SubjectCourse.new
    end

    def create
        @subject_course = SubjectCourse.new(subject_course_params)

        respond_to do |format|
            if @subject_course.save
                format.html { redirect_to new_subject_course_path notice: 'Course and Subject was successfully created.' }
                format.json { render :show, status: :created, location: @subject_course }
            else
                format.html { render :new }
                format.json { render json: @subject_course.errors, status: :unprocessable_entity }
            end
        end
    end

    private

    def subject_course_params
        params.require(:subject_course).permit(:course_id, :subject_id)
    end

end
