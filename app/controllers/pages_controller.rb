class PagesController < ApplicationController
  def home
    if user_signed_in? && current_user.kind == 'admin'
      redirect_to action: "homeadmin"
    end

    if user_signed_in? && current_user.kind == 'standard'
      redirect_to action: "homestandard"
    end

    if user_signed_in? && current_user.kind == 'professor'
      redirect_to action: "homeprofessor"
    end

    if user_signed_in? && current_user.kind == 'director'
      redirect_to action: "homedirector"
    end
  end

  def homeadmin
  end

  def homestandard
  end

  def homeprofessor
  end

  def homedirector
    @departments = Department.all
    @classrooms = Classroom.all
    @subjects = Subject.all
  end
end
