class ClassroomsController < ApplicationController
  before_action :set_classroom, only: [:show, :edit, :update, :destroy]

  # GET /classrooms
  # GET /classrooms.json
  def index
    @classrooms = Classroom.all
  end

  # GET /classrooms/1
  # GET /classrooms/1.json
  def show
  end

  # GET /classrooms/new
  def new
    @teachers = Teacher.all  #Utilizado para Fazer o Select no form de criar e editar Classrooms
    @periods = Period.all  #Utilizado para Fazer o Select no form de criar e editar Classrooms
    @subjects = Subject.all  #Utilizado para Fazer o Select no form de criar e editar Classrooms
    @classroom = Classroom.new
  end

  # GET /classrooms/1/edit
  def edit
    @teachers = Teacher.all  #Utilizado para Fazer o Select no form de criar e editar Classrooms
    @periods = Period.all  #Utilizado para Fazer o Select no form de criar e editar Classrooms
    @subjects = Subject.all  #Utilizado para Fazer o Select no form de criar e editar Classrooms
  end

  # POST /classrooms
  # POST /classrooms.json
  def create
    @classroom = Classroom.new(classroom_params)
    @teachers = Teacher.all  #Utilizado para Fazer o Select no form de criar e editar Classrooms
    @periods = Period.all  #Utilizado para Fazer o Select no form de criar e editar Classrooms
    @subjects = Subject.all  #Utilizado para Fazer o Select no form de criar e editar Classrooms

    respond_to do |format|
      if @classroom.save
        format.html { redirect_to @classroom, notice: 'Classroom was successfully created.' }
        format.json { render :show, status: :created, location: @classroom }
      else
        format.html { render :new }
        format.json { render json: @classroom.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /classrooms/1
  # PATCH/PUT /classrooms/1.json
  def update
    respond_to do |format|
      if @classroom.update(classroom_params)
        format.html { redirect_to @classroom, notice: 'Classroom was successfully updated.' }
        format.json { render :show, status: :ok, location: @classroom }
      else
        format.html { render :edit }
        format.json { render json: @classroom.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classrooms/1
  # DELETE /classrooms/1.json
  def destroy
    @classroom.destroy
    respond_to do |format|
      format.html { redirect_to classrooms_url, notice: 'Classroom was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_classroom
      @classroom = Classroom.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def classroom_params
      params.require(:classroom).permit(:number, :period_id, :teacher_id, :subject_id)
    end
end
