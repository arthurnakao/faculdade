class StudentClassroomsController < ApplicationController

    def new
        @student_classroom = StudentClassroom.new
    end

    def create
        @student_classroom = StudentClassroom.new(student_classroom_params)

        respond_to do |format|
            if @student_classroom.save
                format.html { redirect_to new_student_classroom_path notice: 'Classroom and student was successfully created.' }
                format.json { render :show, status: :created, location: @student_classroom }
            else
                format.html { render :new }
                format.json { render json: @student_classroom.errors, status: :unprocessable_entity }
            end
        end
    end

    private
    def student_classroom_params
        params.require(:student_classroom).permit(:classroom_id, :student_id)
    end

end
