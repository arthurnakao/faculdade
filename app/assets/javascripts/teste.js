const printar = function(event){
    alert("Mensagem: "+event.data.message);
}

$(document).on('turbolinks:load', function() {
    console.log("Chamei");

    $("#botaozin").on('ajax:send', {message: 'Enviado!'}, printar);

    $("#botaozin").on("ajax:success", {message: 'Sucesso!'}, printar)
    .on('ajax:error', {message: 'Errou!'}, printar);

});